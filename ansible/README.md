# **Ansible**

## **Getting Started**

1. Install and Configure AWS CLI: https://docs.aws.amazon.com/cli/latest/userguide/installing.html
     * configure credentials for IAM role authroized
2. Download and Install Ansible: http://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

## **Create Some Instances**

```bash
sudo -H pip install -U boto3
export AWS_REGION="us-east-2"
./site.yml
```

## **Tips**

### **Gathering Info About Infrastructure**

If you would like to see the AWS resources (sg, vpc, vpc subnets) in Python structure that Ansible uses, you can run the following:

```bash
ansible-playbook playbooks/get_info.yml
```
