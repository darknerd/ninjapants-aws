# **AWS Tooling**

This is a guide to how to create AWS EC2 instances.

## **Getting Started**

1. Create AWS Account
1. [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
1. Create IAM Role with Administrative privileges
   * enabled aws cli with that IAM role, download or save credentials (id and key)
1. Configure AWS CLI with those credentials (id and key)
   * select `us-east-2` for region and output in `json`

## **Generate SSH Key Pair**

Generate an SSH key pair that will use deployments and configuration of EC2 instances. A sample script is provided to demonstrate how to do this:

```bash
./aws-genkey.sh
```

## **Create Instances**

Create instances with one of these tools:

1. [aws-cli](aws-cli/README.md)
* [terraform](terraform/README.md)
* [ansible](ansible/README.md)


## **Gathering EC2 Instance Info (Optional)**

You can print out the list of your systems using `aws ec2 describe-instances`.  Here's a small example:

```bash
pretty_describe_instances() {
  # list ec2 instances for current region
  DATA=$(aws ec2 describe-instances \
    --filters 'Name=instance-state-name,Values=running' \
    --query 'Reservations[].Instances[].[Tags[?Key==`Name`].Value[],PrivateIpAddress,PublicIpAddress]' \
    --output text | sed '$!N;s/\n/ /' | tr '\t' ' ')

  # pretty formatted columns
  printf "%-15s %-15s %-15s\n" Name 'Internal IP' 'External IP'
  printf "%-15s %-15s %-15s\n" ---- ----------- -----------
  while read -r I E N; do printf "%-15s %-15s %-15s\n" $N $I $E; done <<< "${DATA}"
}

pretty_describe_instances
```

## **Logging into EC2 Instance**

After this, you can log into these systems with:

```bash
ssh -i .sekrets/aws.pem ubuntu@$EXTERNAL_IP_ADDRESS
```

## **BootStrap Nodes with Chef**

This script depends on parent directory and boostrap script: `../config/boostrap.sh` for this script to work.  The parent script will gather list of systems to configure, then appropriately configure them based on their `Name` tag.  The systems with a tag value of `elasticsearch` will be configured with appropriate unicast discovery list.

```bash
./aws-bootstrap.sh
```
