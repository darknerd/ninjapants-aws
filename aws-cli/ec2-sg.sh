#!/usr/bin/env bash
SG_NAMES="dk-pub-ssh dk-standard"

for SG_NAME in ${SG_NAMES}; do
  aws ec2 create-security-group \
    --group-name ${SG_NAME} \
    --description ${SG_NAME}

  # Fetch GroupID of current Security Group
  SG_ID=$(aws ec2 describe-security-groups \
           --filter "Name=group-name,Values=${SG_NAME}" \
           --query 'SecurityGroups[].GroupId' \
           --output text)

  # Add Tags to Relate them together
  aws ec2 create-tags \
    --resources ${SG_ID} \
    --tags "Key=Type,Value=dk-security" "Key=Name,Value=${SG_NAME}"

  # Create Rules based on group name
  if [[ $SG_NAME =~ "dk-pub-ssh" ]]; then
    aws ec2 authorize-security-group-ingress  \
      --group-id ${SG_ID} \
      --protocol tcp \
      --port 22 \
      --cidr 0.0.0.0/0
  elif [[ $SG_NAME =~ "dk-standard" ]]; then
    CIDR=$(aws ec2 describe-vpcs \
             --filters 'Name=isDefault,Values=true' \
             --query 'Vpcs[].CidrBlock' \
             --output text)
    aws ec2 authorize-security-group-ingress  \
      --group-id ${SG_ID} \
      --protocol -1 \
      --cidr $CIDR
  fi
done
