#!/usr/bin/env bash
[[ -z "${SECURITY_GROUPS}" ]] && { echo 'Error: SECURITY_GROUP_ID not set. Exiting' 1>&2; exit 1; }

declare -A ZONES
declare -A SUBNETS

# Static Variables
KEYNAME="dk-aws"
IMAGE_ID="ami-ee6f5e8b"  # Ubuntu 14.04 in us-east-2
ZONES+=( ["dk-prod-es-01"]="us-east-2a" ["dk-prod-es-02"]="us-east-2b" ["dk-prod-es-03"]="us-east-2c" )

# Build subnet associative array based on default vpc-id
VPC_ID="$(aws ec2 describe-vpcs \
           --filters 'Name=isDefault,Values=true' \
           --query 'Vpcs[].VpcId' \
           --output text)"
SUBNET_TABLE="$(aws ec2 describe-subnets \
            --filter "Name=vpc-id,Values=${VPC_ID}" \
            --query 'Subnets[].[AvailabilityZone,SubnetId]' \
            --output text)"
while read AZ SUBNET; do
  SUBNETS["${AZ}"]="${SUBNET}"
done <<< "${SUBNET_TABLE}"

# Creates 3 elasticsearch nodes across 3 AZs for HA
for EC2_INSTANCE_NAME in dk-prod-es-0{1..3}; do
  # Lookup designated availability zone
  AVAIL_ZONE=${ZONES[$EC2_INSTANCE_NAME]}
  # Create 3 instances
  aws ec2 run-instances \
    --associate-public-ip-address \
    --image-id ${IMAGE_ID} \
    --count 1 \
    --key-name ${KEYNAME} \
    --security-group-ids ${SECURITY_GROUPS} \
    --instance-type 't2.small' \
    --subnet-id ${SUBNETS[$AVAIL_ZONE]} \
    --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=${EC2_INSTANCE_NAME}},{Key=Type,Value=elasticsearch}]"
done
