#!/usr/bin/env bash
[[ -z "${SECURITY_GROUPS}" ]] && { echo 'Error: SECURITY_GROUP_ID not set. Exiting' 1>&2; exit 1; }

# Global Static Variables
KEYNAME="dk-aws"
AVAIL_ZONE="us-east-2a"
IMAGE_ID="ami-ee6f5e8b"  # Ubuntu 14.04 in us-east-2
EC2_INSTANCE_NAME='dk-prod-tools'

# Build subnet associative array based on default vpc-id
VPC_ID="$(aws ec2 describe-vpcs \
           --filters 'Name=isDefault,Values=true' \
           --query 'Vpcs[].VpcId' \
           --output text)"
SUBNET_TABLE="$(aws ec2 describe-subnets \
            --filter "Name=vpc-id,Values=${VPC_ID}" \
            --query 'Subnets[].[AvailabilityZone,SubnetId]' \
            --output text)"
declare -A SUBNETS
while read AZ SUBNET; do
  SUBNETS["${AZ}"]="${SUBNET}"
done <<< "${SUBNET_TABLE}"

### create tools system
aws ec2 run-instances \
  --associate-public-ip-address \
  --image-id ${IMAGE_ID} \
  --count 1 \
  --key-name ${KEYNAME} \
  --security-group-ids ${SECURITY_GROUPS} \
  --instance-type "t2.micro" \
  --subnet-id ${SUBNETS[$AVAIL_ZONE]} \
  --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=${EC2_INSTANCE_NAME}}]"
