#####################################################################
# Security Groups
#####################################################################
resource "aws_security_group" "tf-standard" {
  name        = "tf-standard"
  description = "tf-standard"

  ingress {
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      cidr_blocks     = ["172.31.0.0/16"]
  }

  egress {
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
      "Name" = "tf-standard"
      "Type" = "tf-security"
  }
}

resource "aws_security_group" "tf-pub-ssh" {
  name        = "tf-pub-ssh"
  description = "tf-pub-ssh"

  ingress {
      from_port       = 22
      to_port         = 22
      protocol        = "tcp"
      cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
      "Name" = "tf-pub-ssh"
      "Type" = "tf-security"
  }
}
