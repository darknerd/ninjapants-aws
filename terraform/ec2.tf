#####################################################################
# Variables
#####################################################################
variable "ami-ubuntu14" {
  default = "ami-ee6f5e8b"
  description = "ubuntu 14 ami for us-east-2"
}

#####################################################################
# Instances
#####################################################################
resource "aws_instance" "dk-prod-tools" {
  ami = "${var.ami-ubuntu14}"
  instance_type = "t2.micro"
  key_name = "dk-aws"
  subnet_id = "${aws_subnet.subnet-us-east-2a.id}"

  vpc_security_group_ids = [
     "${aws_security_group.tf-standard.id}",
     "${aws_security_group.tf-pub-ssh.id}"
  ]
  associate_public_ip_address = true

  tags {
    Name = "dk-prod-tools"
  }
}

resource "aws_instance" "dk-prod-es-01" {
  ami                         = "${var.ami-ubuntu14}"
  instance_type               = "t2.small"
  key_name                    = "dk-aws"
  subnet_id                   = "${aws_subnet.subnet-us-east-2a.id}"

  vpc_security_group_ids = [
     "${aws_security_group.tf-standard.id}",
     "${aws_security_group.tf-pub-ssh.id}"
  ]
  associate_public_ip_address = true

  tags {
      "Name" = "dk-prod-es-01"
      "Type" = "elasticsearch"
  }
}

resource "aws_instance" "dk-prod-es-02" {
  ami                         = "${var.ami-ubuntu14}"
  instance_type               = "t2.small"
  key_name                    = "dk-aws"
  subnet_id                   = "${aws_subnet.subnet-us-east-2b.id}"

  vpc_security_group_ids = [
     "${aws_security_group.tf-standard.id}",
     "${aws_security_group.tf-pub-ssh.id}"
  ]
  associate_public_ip_address = true

  tags {
      "Name" = "dk-prod-es-02"
      "Type" = "elasticsearch"
  }
}

resource "aws_instance" "dk-prod-es-03" {
  ami                         = "${var.ami-ubuntu14}"
  instance_type               = "t2.small"
  key_name                    = "dk-aws"
  subnet_id                   = "${aws_subnet.subnet-us-east-2c.id}"

  vpc_security_group_ids = [
     "${aws_security_group.tf-standard.id}",
     "${aws_security_group.tf-pub-ssh.id}"
  ]
  associate_public_ip_address = true

  tags {
      "Name" = "dk-prod-es-03"
      "Type" = "elasticsearch"
  }
}
