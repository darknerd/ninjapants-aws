#!/usr/bin/env bash
KEYPATH=".sekrets"
KEYNAME="dk-aws"

# Generate Key Pair
openssl genrsa -out "${KEYPATH}/aws.pem" 4096
openssl rsa -in "${KEYPATH}/aws.pem" -pubout > "${KEYPATH}/aws.pub"
chmod 400 "${KEYPATH}/aws.pem"

# Install Keys into Metadata
aws ec2 import-key-pair \
  --key-name ${KEYNAME} \
  --public-key-material "$(grep -v PUBLIC .sekrets/aws.pub | tr -d '\n')"

# Backup Keys to ~/.ssh/
cp ${KEYPATH}/aws.pem ${HOME}/.ssh
cp ${KEYPATH}/aws.pub ${HOME}/.ssh
